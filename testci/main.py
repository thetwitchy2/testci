import click


@click.group()
def testci_cli():
    pass


@testci_cli.command()
@click.option('--count', default=1, help='Number of greetings.')
@click.option('--name', prompt='Your name',
              help='The person to greet.')
def hello(count, name):
    """Simple program that greets NAME for a total of COUNT times."""
    for x in range(count):
        click.echo('Hello %s!' % name)
    print("The Return of the Feature")
    # comment
    print("The D Feature")


if __name__ == '__main__':
    testci_cli()
