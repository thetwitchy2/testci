import setuptools
from testci import VERSION

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="testci",
    author="TheTwitchy",
    version=VERSION,
    author_email="thetwitchy@thetwitchy.com",
    description="Testnig ground for CI integration",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    python_requires=">=3.7",
    install_requires=[
        "click",
        "rich",
    ],
    entry_points={
        "console_scripts": [
            "testci = testci.main:testci_cli",
        ],
    },
)
