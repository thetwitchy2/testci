# Changelog

<!--next-version-placeholder-->

## v1.5.0 (2021-05-12)
### Feature
* Feature bump C ([`85b885b`](https://gitlab.com/TheTwitchy/testci/-/commit/85b885b37da26e806578bbd9fe6a10b463a270fb))

## v1.4.0 (2021-05-12)
### Feature
* Test for feature B ([`bc9e96b`](https://gitlab.com/TheTwitchy/testci/-/commit/bc9e96bbda1e9ff9b644a94746821fe3ae5fbc39))

## v1.3.0 (2021-05-11)
### Feature
* The a feature commit ([`1cd073d`](https://gitlab.com/TheTwitchy/testci/-/commit/1cd073d92c60cbf10f4ac561a194e94663a9e4e0))

## v1.2.0 (2021-05-11)
### Feature
* Return of the feature ([`a648993`](https://gitlab.com/TheTwitchy/testci/-/commit/a648993c9e30e834fed13f2a6bd4444be5f27060))

## v1.1.0 (2021-05-11)
### Feature
* The better feature ([`55fba95`](https://gitlab.com/TheTwitchy/testci/-/commit/55fba95743236287359eb1705bc86dab5f0caa21))

## v1.0.0 (2021-05-11)
### Feature
* The breaking change ([`81f0d5f`](https://gitlab.com/TheTwitchy/testci/-/commit/81f0d5f359b9194863d9ad79f85096852898d139))

### Breaking
* the breaking change ([`81f0d5f`](https://gitlab.com/TheTwitchy/testci/-/commit/81f0d5f359b9194863d9ad79f85096852898d139))

## v0.1.0 (2021-05-11)
### Feature
* New dumb file ([`38d0918`](https://gitlab.com/TheTwitchy/testci/-/commit/38d0918487755998d3617180c9397fc7f2ea730f))
